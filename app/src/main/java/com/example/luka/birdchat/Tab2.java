package com.example.luka.birdchat;

/**
 * Created by luka on 17.5.2015..
 */

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import java.util.ArrayList;
import java.util.List;


public class Tab2 extends Fragment {

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout refreshLayout;
    WifiApManager wifiApManager;
    Handler handler = new Handler();



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wifiApManager = new WifiApManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab2, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.client_recycleView);

        refreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        refreshLayout.setColorSchemeColors(R.color.ColorPrimary);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scan();
                        refreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }

        });


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);





        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }


    private List<ClientScanResult> createList(int size) {

        List<ClientScanResult> result = new ArrayList<ClientScanResult>();
        for (int i = 1; i <= size; i++) {
            ClientScanResult ci = new ClientScanResult("IP: 192.168.12.12", "HWAddress: FF:FF:FF:FF:FF:FF", "DEVICE: ", true);
            result.add(ci);
        }
        return result;
    }

    private void scan() {
        wifiApManager.getClientList(false, new FinishScanListener() {

            @Override
            public void onFinishScan(final ArrayList<ClientScanResult> clients) {

                List<ClientScanResult> result = new ArrayList<ClientScanResult>();

                for (ClientScanResult clientScanResult : clients) {

                    ClientScanResult ci = new ClientScanResult("IP Address:" + clientScanResult.getIpAddr(),"Device: " + clientScanResult.getDevice(),"MAC Address: " + clientScanResult.getHWAddr(), clientScanResult.isReachable());
                    result.add(ci);

                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                mRecyclerView.setLayoutManager(layoutManager);

                AdapterClient mAdapter = new AdapterClient(result);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);

            }
        });

    }



}