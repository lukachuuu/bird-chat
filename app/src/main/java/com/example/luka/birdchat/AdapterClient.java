package com.example.luka.birdchat;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

/**
 * Created by luka on 18.5.2015..
 */
public class AdapterClient extends RecyclerView.Adapter<AdapterClient.ClientViewHolder> {

    private List<ClientScanResult> clientList;
    private Context mContext;

    public AdapterClient(List<ClientScanResult> clientList) {
        this.clientList = clientList;
    }


    @Override
    public int getItemCount() {
        return clientList.size();
    }

    @Override
    public void onBindViewHolder(ClientViewHolder clientViewHolder, int i) {
        ClientScanResult ci = clientList.get(i);
        clientViewHolder.vIP.setText(ci.getIpAddr());
        clientViewHolder.vDevice.setText(ci.getDevice());
        clientViewHolder.vHwAddress.setText(ci.getHWAddr());
        clientViewHolder.vIisReachable.setText(ci.isReachable()+"");
    }

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.client_item, viewGroup, false);

        return new ClientViewHolder(itemView);
    }

    public static class ClientViewHolder extends RecyclerView.ViewHolder {

        protected CardView cv;
        protected TextView vIP;
        protected TextView vDevice;
        protected TextView vHwAddress;
        protected TextView vIisReachable;

        public ClientViewHolder(View v) {
            super(v);
            cv = (CardView)itemView.findViewById(R.id.cv);
            vIP =  (TextView) v.findViewById(R.id.ip_address);
            vDevice = (TextView)  v.findViewById(R.id.device);
            vHwAddress = (TextView)  v.findViewById(R.id.HWAddr);
            vIisReachable = (TextView) v.findViewById(R.id.isReachable);
        }
    }

}

