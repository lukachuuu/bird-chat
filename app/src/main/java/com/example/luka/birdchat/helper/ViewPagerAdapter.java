package com.example.luka.birdchat.helper;

/**
 * Created by luka on 17.5.2015..
 */import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.luka.birdchat.Tab1;
import com.example.luka.birdchat.Tab2;
import com.example.luka.birdchat.Tab3;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0)
        {
            Tab1 tab1 = new Tab1();
            return tab1;
        }
        else if(position == 1)
        {
            Tab3 tab3 = new Tab3();
            return tab3;
        }
        else if(position == 2)
        {
            Tab2 tab2 = new Tab2();
            return tab2;
        }

        else
        {
            return null;
        }


    }


    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }


    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
