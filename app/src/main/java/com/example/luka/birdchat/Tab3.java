package com.example.luka.birdchat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


/**
 * Created by luka on 17.5.2015..
 */
public class Tab3 extends Fragment {

    private EditText message;
    private EditText port;
    private EditText hostAddress;
    private Button bttnSend;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.tab3, container, false);
        return v;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bttnSend = (Button) view.findViewById(R.id.bttn_send);
        message = (EditText) view.findViewById(R.id.message);
        port = (EditText) view.findViewById(R.id.port);
        hostAddress = (EditText) view.findViewById(R.id.sendToIp);

        bttnSend.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                //ovo se ne radi tak al funkcionira

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            String messageStr = message.getText().toString();
                            int server_port = Integer.parseInt(port.getText().toString());
                            DatagramSocket s = new DatagramSocket();
                            InetAddress local = InetAddress.getByName(hostAddress.getText().toString());//my broadcast ip
                            int msg_length = messageStr.length();
                            byte[] message = messageStr.getBytes();
                            DatagramPacket p = new DatagramPacket(message, msg_length, local, server_port);
                            s.send(p);
                            s.close();



                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

                Toast.makeText(getActivity().getBaseContext(), "Poslano", Toast.LENGTH_LONG).show();
            }
        });

    }


}
