package com.example.luka.birdchat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luka on 17.5.2015..
 */
public interface FinishScanListener {


    /**
     * Interface called when the scan method finishes. Network operations should not execute on UI thread
     * @param  ArrayList of {@link ClientScanResult}
     */

    public void onFinishScan(ArrayList<ClientScanResult> clients);

}