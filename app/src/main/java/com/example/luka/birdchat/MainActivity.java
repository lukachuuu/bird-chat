package com.example.luka.birdchat;


import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.luka.birdchat.helper.SlidingTabLayout;
import com.example.luka.birdchat.helper.ViewPagerAdapter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class MainActivity extends ActionBarActivity {

    private Toolbar toolbar;
    WifiApManager wifiApManager;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Talk", "Chat", "Devices"};
    int Numboftabs = 3;
    private AudioCall call;
    private boolean activeCall = true;
    public static String myIP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Bird Chat");
        setSupportActionBar(toolbar);
        myIP = getLocalIpAddress();


//        try {
//            Toast.makeText(getBaseContext(), getBroadcastAddress().toString(), Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//            Log.d("GRESKA", e.getMessage().toString());
//        }

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        tabs.setViewPager(pager);

        Thread recive = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatagramSocket socket = new DatagramSocket(5555);       //Fiksni port za dobivanje poruke
                    byte[] buf = new byte[1024];
                    while (true) {
                        DatagramPacket packet = new DatagramPacket(buf, buf.length);
                        socket.receive(packet);
                        final String tmp = new String(packet.getData(), 0, packet.getData().length - 1, "US-ASCII");
                        posaljiNotifikaciju(tmp);
                        Log.d("PAKET", tmp);

                    }
                } catch (SocketException e) {
                    Log.e("SocketException", e.getMessage());

                } catch (IOException e) {
                    Log.e("IOException", e.getMessage());
                }


            }
        });

        recive.start();

        wifiApManager = new WifiApManager(this);

    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            call = new AudioCall(getBroadcastAddress(), getLocalIpAddress());
            call.startCall();
        } catch (Exception e) {
            Log.e("OnStartFail", e.getMessage());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //nastavljanje s radom aplikacije u slucaju stavljanja aplikacije u background
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.open_ap:
                Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show();
                wifiApManager.setWifiApEnabled(null, true);
                return true;

            case R.id.close_ap:
                Toast.makeText(this, "Close", Toast.LENGTH_SHORT).show();
                wifiApManager.setWifiApEnabled(null, false);
                return true;

            case R.id.action_ip:
                Toast.makeText(this, getLocalIpAddress().toString(), Toast.LENGTH_LONG).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private InetAddress getBroadcastAddress() throws IOException {
        WifiManager myWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        DhcpInfo myDhcpInfo = myWifiManager.getDhcpInfo();
        if (myDhcpInfo == null) {
            System.out.println("Could not get broadcast address");
            return null;
        }
        int broadcast = (myDhcpInfo.ipAddress & myDhcpInfo.netmask) | ~myDhcpInfo.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);

    }

    public void pauseNesto(View v) {

        ImageButton imageBttn = (ImageButton) findViewById(R.id.imageButton);

        if (activeCall) {
            call.endCall();
            imageBttn.setImageResource(R.drawable.call_down);
            Toast.makeText(this, "Conversation: INACTIVE", Toast.LENGTH_SHORT).show();
            activeCall = false;
        } else {
            call.startCall();
            imageBttn.setImageResource(R.drawable.call);
            Toast.makeText(this, "Conversation: ACTIVE", Toast.LENGTH_SHORT).show();
            activeCall = true;
        }

    }

    public String getLocalIpAddress() {
        WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        return ip;
    }

    public String getWifiApIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en
                    .hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                if (intf.getName().contains("wlan")) {
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                            .hasMoreElements(); ) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()
                                && (inetAddress.getAddress().length == 4)) {
                            Log.d("AP ADDRESS: ", inetAddress.getHostAddress());
                            return inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("EX", ex.toString());
        }
        return null;
    }

    public void posaljiNotifikaciju(String poruka) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(android.R.drawable.ic_dialog_email)
                .setLargeIcon(
                        BitmapFactory.decodeResource(getResources(),
                                android.R.drawable.ic_menu_send))
                .setContentTitle("Poruka")
                .setContentText(poruka)
                .setTicker("BirdChat - primljena nova poruka")
                .setVibrate(new long[]{0, 100, 200, 300, 400, 500});


        Notification notification = builder.build();
        notification.defaults = Notification.DEFAULT_SOUND;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);
    }
}
